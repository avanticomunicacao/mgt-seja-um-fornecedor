<?php
namespace Avanti\SejaUmFornecedor\Controller\Index;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Avanti\SejaUmFornecedor\Model\ConfigInterface;
use Avanti\SejaUmFornecedor\Model\MailInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;


class Post extends \Avanti\SejaUmFornecedor\Controller\Index implements HttpPostActionInterface
{
    const TYPE_DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    const TYPE_DOC = 'application/msword';
    const TYPE_PDF = 'application/pdf';
    const TYPE_XLS = 'application/vnd.ms-excel';
    const TYPE_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

    /**
     * @var Context
     */
    private $context;

    /**
     * @var MailInterface
     */
    private $mail;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Context $context
     * @param ConfigInterface $contactsConfig
     * @param MailInterface $mail
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig,
        MailInterface $mail,
        LoggerInterface $logger = null
    ) {
        parent::__construct($context, $contactsConfig);
        $this->context = $context;
        $this->mail = $mail;
        $this->logger = $logger ?: ObjectManager::getInstance()->get(LoggerInterface::class);
    }

    /**
     * Post user question
     *
     * @return Redirect
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        try {
            $this->sendEmail($this->validatedParams());
            $this->messageManager->addSuccessMessage(
                __('Thanks for contacting us. We\'ll respond to you very soon.')
            );
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(
                __($e->getMessage())
            );
        }

        return $this->resultRedirectFactory->create()->setPath('seja-um-fornecedor');
    }

    /**
     * @param array $post Post data from contact form
     * @return void
     */
    private function sendEmail($post)
    {
        $result = $post->getParams();

        if ($post->getFiles()) {
            $files = $post->getFiles()->getArrayCopy();
            if ($files['upload_file']['size'] > 0) {
                $data = $post->getParams();
                $result = array_merge($files, $data);

                $this->validateFile($files);
            }
        }

        $this->mail->send(
            $result['email'],
            ['data' => new DataObject($result)]
        );
    }

    private function validateFile($file)
    {
        $types = array(self::TYPE_DOC, self::TYPE_DOCX, self::TYPE_PDF, self::TYPE_XLS, self::TYPE_XLSX);
        $type = $file['upload_file']['type'];

        $return = array_search($file, $types);

        if ($return) {
            return true;
        }

        throw new \Exception(__("This File is not supported"));
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if (trim($request->getParam('name')) === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if (false === \strpos($request->getParam('email'), '@')) {
            throw new LocalizedException(__('The email address is invalid. Verify the email address and try again.'));
        }
        if (trim($request->getParam('hideit')) !== '') {
            throw new \Exception();
        }

        return $request;
    }
}
