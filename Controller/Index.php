<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Avanti\SejaUmFornecedor\Controller;

use Avanti\SejaUmFornecedor\Model\ConfigInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NotFoundException;

/**
 * Contact module base controller
 */
abstract class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var ConfigInterface
     */
    private $contactsConfig;

    /**
     * @param Context $context
     * @param ConfigInterface $contactsConfig
     */
    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig
    ) {
        parent::__construct($context);
        $this->contactsConfig = $contactsConfig;
    }

    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        return parent::dispatch($request);
    }
}
