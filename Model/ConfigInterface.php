<?php

namespace Avanti\SejaUmFornecedor\Model;

/**
 * Contact module configuration
 *
 * @api
 * @since 100.2.0
 */
interface ConfigInterface
{
    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT_1 = 'avanti_fornecedor/general/email1';

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT_2 = 'avanti_fornecedor/general/email2';

    /**
     * Sender email config path
     */
    const XML_PATH_EMAIL_SENDER = 'contact/email/sender_email_identity';

    /**
     * Return email sender address
     *
     * @return string
     * @since 100.2.0
     */
    public function emailSender();

    /**
     * Return email recipient address
     *
     * @return string
     * @since 100.2.0
     */
    public function emailRecipient($fornecedor);
}
