<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Avanti\SejaUmFornecedor\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;


/**
 * Contact module configuration
 */
class Config implements ConfigInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function emailSender()
    {
        return $this->scopeConfig->getValue(
            ConfigInterface::XML_PATH_EMAIL_SENDER,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * {@inheritdoc}
     */
    public function emailRecipient($fonecedor)
    {
        $emailRecipient = $this->scopeConfig->getValue(
            ConfigInterface::XML_PATH_EMAIL_RECIPIENT_1,
            ScopeInterface::SCOPE_STORE
        );

        if ($fonecedor== 'Material Revenda') {
            $emailRecipient = $this->scopeConfig->getValue(
                ConfigInterface::XML_PATH_EMAIL_RECIPIENT_2,
                ScopeInterface::SCOPE_STORE
            );
        }

        return $emailRecipient;
    }
}
