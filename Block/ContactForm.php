<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Avanti\SejaUmFornecedor\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Cms\Model\PageFactory;

class ContactForm extends Template
{
    protected $scopeConfig;

    /**
     * Page factory
     *
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = [],
        ScopeConfigInterface $scopeConfig,
        PageFactory $pageFactory)
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->scopeConfig = $scopeConfig;
        $this->pageFactory = $pageFactory;
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('seja-um-fornecedor/index/post', ['_secure' => true]);
    }

    public function getMessageBlock()
    {
        return $this->getStoreConfig("avanti_fornecedor/general/cms_block_template_message");
    }

    private function getStoreConfig($path)
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue($path, $storeScope);
    }

    protected function _prepareLayout()
    {
        $page = $this->getPage();
        $this->pageConfig->getTitle()->set(__("Become a Supplier"));

        return parent::_prepareLayout();
    }

    /**
     * Retrieve Page instance
     *
     * @return \Magento\Cms\Model\Page
     */
    public function getPage()
    {
        $page = $this->pageFactory->create();
        $this->setData('page', $page);
        return $this->getData('page');
    }
}
